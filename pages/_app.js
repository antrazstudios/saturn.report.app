import { ChakraProvider } from '@chakra-ui/react'
import '../styles/globals.css'
import style from '../styles/Layout.module.css'
import Toolbar from '../components/toolbar'

function MyApp({ Component, pageProps }) {
  return (<ChakraProvider>
    <div className={style.app}>
      <header><Toolbar/></header>
      <main className={style.main}>
        <aside className={style.aside}>Prueba de otro</aside>
        <div className={style.component}>
          <Component {...pageProps} />
        </div>
      </main>
      <footer>algo</footer>
    </div>
  </ChakraProvider>)
}

export default MyApp
